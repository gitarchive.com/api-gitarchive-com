const NativeRequest = require('../lib/api-request.js')
const Dataset = require('./top.1000.sites.list.js')

function followRedirect(url, callback)
{
    return new NativeRequest(url, (data) => {
        if (!data.headers['location'])
            { callback(url) }
        else   
            { return followRedirect(data.headers['location'], callback) }
    })
}

let i = 0; while (i<200) { Dataset.shift(); i++; }

while (Dataset.length)
{
    followRedirect(Dataset[0], function (url) {
        console.log(url)
    })

    Dataset.shift()
}

