const fs = require('fs')
const config = require("../../config");
const APIRequest = require("../../lib/api_request");
const Dataset = require('./indiehackers.products.dataset')

let n = 0;
let i = 0;
let ProductsList = []

async function	addResources () {
	const url = ProductsList[i]

	if (!url)
		return console.log(new Date().getTime() + ' - Resources adding ended (total: ' + i + ')');
	console.log("try to insert new resource:", url)

	await APIRequest.resource({}).createResource(url)
	.then((res) => { console.log("response succefully created with id:", res._pk) })
	.catch((err) => { console.log("Err:", err) })

	i++;

	return addResources();
}

for (i = 0 ; i < Dataset.length ; i++)
{
	const products = Dataset[i].results[0].hits;
	for (var j = 0 ; j < products.length ; j++)
	{
		const product = products[j]
		ProductsList.push(product.websiteUrl)
	}
}

addResources();

/*
fs.writeFile(
    'indiehackers.products.list.js',
    "module.exports = "+JSON.stringify(Array.from(ProductsList)), 
    (err) => {
        if (err) throw err;
        console.log('The file has been saved!');
    }
);
*/
