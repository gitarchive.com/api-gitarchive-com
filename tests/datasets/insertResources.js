const fs = require('fs')
const config = require("../../config");

const lib = require("gitarchive_lib");
const APIRequest = lib.request;

const argv = process.argv.slice(2);

if (argv.length != 1) {
	console.log("Need a dataset in argument");
	process.exit();
}
const file = require("./" + argv[0])

let n = 0;
let i = 0;
let ProductsList = []

async function	addResources () {
	const url = ProductsList[i]

	if (!url)
		return console.log(new Date().getTime() + ' - Resources adding ended (total: ' + i + ')');
	console.log("try to insert new resource:", url)

	await APIRequest.resource(config.services.api, {}).createResource(url)
	.then((res) => { console.log("response succefully created with id:", res._pk) })
	.catch((err) => { console.log("Err:", err) })

	i++;

	return addResources();
}

for (i = 0 ; i < file.length ; i++)
{
	let products = "";
	if (file[i].results[0].hits)
		products = file[i].results[0].hits;
	else {
		console.log("bad format")
		process.exit(1)
	}
	for (let j = 0 ; j < products.length ; j++)
	{
		const product = products[j]
		ProductsList.push(product.websiteUrl)
	}
}

addResources();
