const Database = require('../lib/database');

Database.any('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\'').catch(e =>
{
  if (e.message === "No data returned from the query.")
    return []
  else
    throw Error(e);
})
.then((tables) => { console.log('Successfully connected to database. Tables list: ', tables) })
