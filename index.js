const express		= require("express");
const bodyParser	= require("body-parser");
const lib			= require('gitarchive_lib');
const app			= express();
const Sentry 		= require('@sentry/node');

const config			= require("./config");
const APIError			= lib.error;
const logger 			= lib.logger;

const AuthenticationController 	= require("./controllers/authentication");
const PermissionsController		= require("./controllers/permissions");
const ScraperController			= require("./controllers/scraper");
const GitController				= require('./controllers/git');
const UsersResourcesController		= require('./controllers/usersResources');
const ResourcesController		= require('./controllers/resources');
const FetchesController			= require('./controllers/fetches');
const CommitsController			= require('./controllers/commits');
const ErrorController			= lib.errorController;

/**
 * Initialize Sentry
 * more: https://docs.sentry.io/platforms/node/express/
 */

Sentry.init({ dsn: config.services.sentry });
app.use(Sentry.Handlers.requestHandler());

/**
 * Log all requests
 */

app.use((req, res, next) => { 

	const remoteAddress = (req.header('X-Forwarded-For') || '').split(',').pop() 
		|| req.connection.remoteAddress
		|| req.socket.remoteAddress
		|| req.connection.socket.remoteAddress;
	
	const remoteUser = req.header('Authorization');
	const userAgent = req.header('User-Agent');

	// ::1 - alpha_f5ad77e32e9126fffa981d218a4ddd1d36fa692b [22/Nov/2018:13:50:38 +0000] "GET / HTTP/1.1" 404 59 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"
	logger.info([remoteAddress, remoteUser, ("\"" + req.method + " " + req.originalUrl + "\""), "\"" + userAgent + "\""].join(" "));
	
	return next();
})

/**
 *	Authenticate requests
 */

app.all("*", AuthenticationController);

/**
 *	Parse body on certain queries
 */

// Apply to request where Content-Type is set as "application/octet-stream" (binary)
// This way, req.body is a buffer (useful for UsercontentModel)
// More at https://github.com/expressjs/body-parser/blob/master/README.md#bodyparserrawoptions
app.use(bodyParser.raw({type: "application/octet-stream", limit: '10mb' }));

// Parse request with Content-Type is set as "application/x-www-form-urlencoded"
// This allow us to use req.body.params :)
// More at https://github.com/expressjs/body-parser/blob/master/README.md#bodyparserrawoptions
app.use(bodyParser.urlencoded({extended: true, type: "application/x-www-form-urlencoded"}));

//app.use(bodyParser.json());

/**
 *	Surcharge express with a response.success method
 */

app.response.success = function (json) {
	
	if (typeof json !== "object")
		throw Error("Unable to use res.success, not a json.");
	
	json.message = (typeof json.message == "string" ? json.message : "succeeded");
	json.statusCode = (typeof json.statusCode == "number" ? json.statusCode : 200);

	this.status(json.statusCode);
	this.json(json);
}

/**
 *	Express routes (Express Auth)
 */

let router = express.Router(); // Create an instance of the express Router

//
// Sanity check route
//

router.get("/", (req, res) => { return res.success({message: "GitArchive API ready - (Version: " + config.services.api.version + ")"}); });

//
// Resources related routes
//

router.get("/users/resources", UsersResourcesController.all)

router.get("/resources", PermissionsController, ResourcesController.all)
router.post("/resources", ResourcesController.create);

router.get("/resources/lastfetch", PermissionsController, ResourcesController.getLastFetched);
router.get("/resources/search", PermissionsController, ResourcesController.search);
router.get("/resources/random", PermissionsController, ResourcesController.random);

router.get("/resources/:resource_id", ResourcesController.one);

router.post("/resources/:resource_id/fetches", PermissionsController, FetchesController.create);

router.get("/resources/:resource_id/commits", CommitsController.all);
router.get("/resources/:resource_id/commits/:commit_id", CommitsController.one);
router.post("/resources/:resource_id/commits", PermissionsController, CommitsController.create);

router.get("/resources/:resource_id/raw/:commit_id/:filename", GitController.getRaw);
router.post("/resources/:resource_id/raw/:filename", PermissionsController, GitController.setRaw);

router.get("/resources/:resource_id/diff/:from_commit_id\.\.:to_commit_id/:filename", GitController.diff);

//
// Scraper related routes.
//

router.get("/scraper/fetch", ScraperController.getFetch);

/**
 *	Config
 */

app.set('port', config.services.api.port)
app.use("/v" + config.services.api.version, router); // Prefix of routes
app.listen(app.get('port')); // Listen port

logger.info("Server started and connected ! API listen on port: " + app.get('port'));

/**
 * Error handling
 */

//
// 404 responses
// more: expressjs.com/en/starter/faq.html#how-do-i-handle-404-responses
//

app.use((req, res, next) => { throw APIError.notFound('Not found'); });

//
// Sentry error handler must be before any other error middleware
// more: https://docs.sentry.io/platforms/node/express/
//

app.use(Sentry.Handlers.errorHandler());

//
// Error handler
//

app.use(ErrorController);
