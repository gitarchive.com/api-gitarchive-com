const ResourcesModel = require('../../models/resources')
const lib = require('gitarchive_lib');
const APIError = lib.error;

/*
 *	Return resources (with latest commits) mathing with url given
 */

module.exports = async function (req, res, next) {

	let query 	= (req.query.q ? req.query.q : null);
	const limit = (Number(req.query.limit) ? req.query.limit : null);
	
	if (!query)
		{ return next(APIError.badRequest('Missing q parameter.')); }

	query = query.trim();

	if (query === "")
		{ return next(APIError.badRequest('Q parameter can\'t be empty')); }

	ResourcesModel.search(limit, query)
	.then((ResourcesList) => { return res.success(ResourcesList); })
	.catch(next); // Handle errors with express error middleware
};
