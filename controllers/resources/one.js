/**
 *	Return resource information
 *
 * @param: resource's ID
 * @response: ResourceItem
 */

const ResourcesModel = require('../../models/resources');
const lib = require('gitarchive_lib');
const APIError = lib.error;

module.exports = function (req, res, next) {
	const resource = {};
	resource.id = (req.params.resource_id ? req.params.resource_id : null);

	if (!resource.id)
		{ return next(APIError.badRequest("Resource id is missing.")); }
	
	// Validate resource's ID
	if (resource.id.length && resource.id.length !== 64)
		{ return next(APIError.badRequest("Resource id is malformed.")); }

	ResourcesModel.one(resource.id)
	.then(ResourceItem => { return res.success(ResourceItem); })
	.catch(next); // Handle errors with express error middleware
}
