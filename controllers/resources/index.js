const RandomResource 	= require('./random');
const SearchResource 	= require('./search');
const AllResources 		= require('./all');
const LastFetchedResource = require('./lastfetch');
const GetResourceInformation = require('./one');
const CreateResource = require('./create');

/**
 *
 */

const Resources = {

	all: AllResources,
	getLastFetched: LastFetchedResource,
	random: RandomResource,
	search: SearchResource,
	one: GetResourceInformation,
	create: CreateResource,

};

module.exports = Resources;