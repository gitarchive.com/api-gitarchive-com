const ResourcesModel = require('../../models/resources')

module.exports = async function (req, res, next) {

	ResourcesModel.random()
	.then((ResourceItem) => {
		// TODO. This redirection will fail when using auth requests
		// with an API_KEY. Should return a JSON in the future.
		//return res.redirect("/v1/resources/" + ResourceItem.id);
		return res.success(ResourceItem);
	})
	.catch(next); // Handle errors with express error middleware
}
