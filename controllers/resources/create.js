const lib = require('gitarchive_lib');
const APIError = lib.error;
const ResourcesModel = require('../../models/resources');
const UserResourceController = require('../usersResources');
const UserContentModel = require('../../models/usercontent');
const crypto = require('crypto');
const URL = require('url');

module.exports = async function (req, res, next) {

	const resource = {};
	resource.location = req.body.location;

	// Validate location existence (url)
	if (!resource.location)
		{ return next(APIError.badRequest('Resource\'s location is missing.')); }

	// Validate location is an URL
	try { resource.url = URL.parse(resource.location); }
	catch (e) { return next(APIError.badRequest("Resource'\s location is malformed.")); }

	// Confirm location's protocol is supported
	if (!(["http:","https:"].includes(resource.url.protocol)))
		{ return next(APIError.badRequest('Unable to add resource, protocol is not supported.')); }

	// Complete resource object
	resource.id = crypto.createHash('sha256').update(resource.location, 'utf8').digest('hex');
	resource.usercontent = 1;

	// Verify resource existence
	const ResourceItem = await ResourcesModel.one(resource.id)
	.then(ResourceItem => { return ResourceItem; })
	.catch(err => { if (err.statusCode !== 404) { return next(err); } });

	// null will be replace with fetch_time (fetch time delay user want)
	if (ResourceItem && res.locals.role && res.locals.role === "user") {
		return UserResourceController.create(res.locals.pk, ResourceItem, null, next)
		.then(UserResourceItem => { return res.success(UserResourceItem); })
		.catch(err => { if (err.statusCode !== 404) { return next(APIError.badImplementation("Server Error")) } });
	}

	// If resource exists, returns it right away
	if (ResourceItem) { return res.success(ResourceItem); }
	
	// Otherwise, try to create it.
	UserContentModel.createResource(resource)
	.then(() => { return ResourcesModel.create(resource.location, resource.id, resource.usercontent) })
	.then(NewResourceItem => { return ResourcesModel.one(NewResourceItem.id) })
	.then(NewResourceItem => {
		// Add resource to the user's list
		if (res.locals.role && res.locals.role === "user") {
			return UserResourceController.create(res.locals.pk, NewResourceItem, null, next)
			.then(UserResourceItem => { return res.success(UserResourceItem); })
			.catch(err => { if (err.statusCode !== 404) { return next(APIError.badImplementation("Server Error")) } });
		}
		return res.success(NewResourceItem);
	}) // TODO. Return a 201 :)
	.catch(next); // Handle errors with express error middleware

}
