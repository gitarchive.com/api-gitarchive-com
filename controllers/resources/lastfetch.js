const ResourcesModel = require('../../models/resources')

/*
 *	Return last resources fetched
 */

module.exports = function (req, res, next) {
	const limit 		= (Number(req.query.limit) ? req.query.limit : null);

	ResourcesModel.getLatest(limit)
	.then(ResourcesList => { return res.success(ResourcesList); })
	.catch(next); // Handle errors with express error middleware
};
