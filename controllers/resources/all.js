const ResourcesModel = require('../../models/resources')

/*
 *	Return all resources
 */

module.exports = function (req, res, next) {
	
    let limit = (req.query.limit ? req.query.limit : null);

    ResourcesModel.all(limit)
    .then(ResourcesList => { return res.success(ResourcesList); })
    .catch(next); // Handle errors with express error middleware
};
