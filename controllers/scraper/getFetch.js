const ScraperModels = require('../../models/scraper');

/*
 *	Return all resources to fetch
 */

module.exports = function (req, res, next) {

	ScraperModels.getLatest()
	.then(ResourcesList => { return res.success(ResourcesList); })
	.catch(next); // Handle errors with express error middleware
};
