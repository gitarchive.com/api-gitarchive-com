/**
 * Make a new commit.
 *
 * @param resource_id (mandatory)
 *
 * @returns: CommitItem
 */

const lib = require('gitarchive_lib');
const APIError = lib.error;
const ResourcesModel = require('../../models/resources');
const CommitsModel = require('../../models/commits');
const UserContentModel = require('../../models/usercontent');

module.exports = async function (req, res, next) {

	const resource = {}, commit = {};
	resource.id = req.params.resource_id;

	// Validate location existence (url)
	if (!resource.id)
		{ return next(APIError.badRequest('Resource\'s id is missing.')); }

	if (resource.id.length !== 64)
		{ return next(APIError.badRequest('Resource\'s id is malformed.')); }

	// Verify resource existence
	const ResourceItem = await ResourcesModel.one(resource.id)
	.catch(err => { return next(err); });

	// Make commit and save it in DB
	UserContentModel.createCommit(ResourceItem)
	.then(UsercontentCommitItem => {
		commit.id = UsercontentCommitItem.id
		return CommitsModel.create(ResourceItem._pk, commit.id);
	})
	.then((dbResult) => { return CommitsModel.one(ResourceItem.id, commit.id); })
	.then(CommitItem => { return res.success(CommitItem); })
	.catch(next); // Handle errors with express error middleware
};
