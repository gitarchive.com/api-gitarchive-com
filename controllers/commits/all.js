/**
 *	Return all commits for a given resource.
 *
 * @param resource_id
 * @returns CommitsList
 */

const CommitsModel = require('../../models/commits')
const lib = require('gitarchive_lib');
const APIError = lib.error;

module.exports = function (req, res, next) {

	const resource = {};
	resource.id = req.params.resource_id;

	let limit = (req.query.limit ? req.query.limit : null);

	// Validate resource's ID
	if (!resource.id)
		{ return next(APIError.badRequest("Resource'id is missing.")); }

	if (resource.id && resource.id.length && resource.id.length !== 64)
		{ return next(APIError.badRequest("Resource's id is malformed.")); }

	// Ask commit's model
  CommitsModel.all(resource.id, limit)
	.then(CommitsList => { return res.success(CommitsList); })
	.catch(next); // Handle errors with express error middleware
};
