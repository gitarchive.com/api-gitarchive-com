/**
 * Return information from a specific commit.
 *
 * @param resource_id
 * @param commit_id
 *
 * @returns CommitItem
 */

const CommitsModel = require('../../models/commits');
const lib = require('gitarchive_lib');
const APIError = lib.error;

module.exports = function (req, res, next) {

	const resource = {};
	resource.id = (req.params.resource_id ? req.params.resource_id : null);

	const commit = {};
	commit.id = (req.params.commit_id ? req.params.commit_id : null);

	// Validate resource's id
	if (!resource.id)
		{ return next(APIError.badRequest("Resource's ID is required.")); }

	if (resource.id && resource.id.length && resource.id.length !== 64)
		{ return next(APIError.badRequest("Resource's ID is malformed.")); }

	// Validate resource's id
	if (!commit.id)
		{ return next(APIError.badRequest("Commit's ID is required.")); }

	if (commit.id && commit.id.length && commit.id.length !== 40)
		{ return next(APIError.badRequest("Commit's ID is malformed.")); }

	// Get commit
	CommitsModel.one(resource.id, commit.id)
	.then(CommitItem => { return res.success(CommitItem); })
	.catch(next);
};
