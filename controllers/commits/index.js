const AllCommits = require('./all');
const GetCommit = require('./one');
const CreateCommit = require('./create');

module.exports = {
	all: AllCommits,
	one: GetCommit,
	create: CreateCommit,
}
