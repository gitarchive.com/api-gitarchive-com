const lib = require('gitarchive_lib');
const APIError = lib.error;
const ResourcesModel = require('../../models/resources');
const FetchesModel = require('../../models/fetches');

module.exports = async function (req, res, next) {

	const resource = {}, response = {}, request = {};

	resource.id = req.params.resource_id;

	response.statusCode 	= req.body.response_status_code;
	response.contentType 	= req.body.response_content_type;
	response.contentLength 	= req.body.response_content_length;

	request.timeRequested = req.body.request_time_requested;
	request.timeResponded = req.body.request_time_responded;
	request.timeCompleted = req.body.request_time_completed;
	request.supplier = req.body.request_supplier;
	
	// Validate location existence (url)
	if (!resource.id)
		{ return next(APIError.badRequest('Resource\'s id is missing.')); }

	if (resource.id.length !== 64)
		{ return next(APIError.badRequest('Resource\'s id is malformed.')); }
	
	// Verify resource existence
	const ResourceItem = await ResourcesModel.one(resource.id)
	.catch(err => { return next(err); });
	
	// Create (save) the fetch
	FetchesModel.create(ResourceItem, request, response)
	.then(FetchItem => { return res.success(FetchItem); })
	.catch(next); // Handle errors with express error middleware
}
