const lib = require('gitarchive_lib');
const APIError = lib.error;
const UserResourceModel = require('../../models/users_resources');

module.exports = async function (api_key_pk, ResourceItem, fetchTime, next) {
	// Verify user resource existence
	const UserResourceItem = await UserResourceModel.one(api_key_pk, ResourceItem._pk)
	.then(UserResourceItem => { return UserResourceItem; })
	.catch(err => { if (err.statusCode !== 404) { return next(APIError.badImplementation("Server Error")); } });

	// If resource exists, returns it right away
	if (UserResourceItem) { return UserResourceItem; }

	// Otherwise, try to create it.
	return UserResourceModel.create(api_key_pk, ResourceItem._pk, null)
	.then(UserResourceItem => { return UserResourceItem })
	.catch(err => { return next(err) });
}
