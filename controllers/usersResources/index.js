const AllResources 		= require('./all');
const CreateUserResource	= require('./create');

/**
 *
 */

const UsersResources = {

	all: AllResources,
	create: CreateUserResource

};

module.exports = UsersResources;
