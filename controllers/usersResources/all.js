const UsersResourcesModel = require('../../models/resources')

/*
 *	Return all resources from particular user
 */

module.exports = function (req, res, next) {
	
    let limit = (req.query.limit ? req.query.limit : null);

    UsersResourcesModel.all(res.local.api_key, limit)
    .then(UsersResourcesList => { return res.success(UsersResourcesList); })
    .catch(next); // Handle errors with express error middleware
};
