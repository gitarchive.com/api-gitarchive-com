const SetRaw = require('./raw_set');
const GetRaw = require('./raw_get');
const GetDiff = require('./diff');

module.exports = {

	diff: GetDiff,
	getRaw: GetRaw,
	setRaw: SetRaw,

};
