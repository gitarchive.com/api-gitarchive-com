/**
 * Handle queries to return raw data of a specific resources.
 * 
 * @param: resource_id (mandatory)
 * @param: commit_hash (optional)
 * 
 * @returns: RAW DATA (not a json)
 */

const ResourcesModel = require('../../models/resources');
const CommitsModel = require('../../models/commits');
const config = require("../../config")
const lib = require('gitarchive_lib');
const APIError = lib.error;

module.exports = async function (req, res, next) {

	const resource 	= {}, commit = {};
	resource.id 	= req.params.resource_id;
	commit.id 	= req.params.commit_id;
	
	const fileName = req.params.filename;

	// Validate resource's id param (url)
	if (!resource.id)
		{ return next(APIError.badRequest('Resource\'s id is missing.')); }
	
	if (resource.id.length !== 64)
		{ return next(APIError.badRequest('Resource\'s id is malformed.')); }
	
	// Validate commit's id param
	if (!commit.id)
		{ return next(APIError.badRequest('Commit\'s id is missing.')); }

	if (commit.id.length !== 40)
		{ return next(APIError.badRequest('Commit\'s id is malformed.')); }
	
	// Validate filename param
	if (!fileName || !(["body", "headers", "dom"].includes(fileName)))
		{ return next(APIError.badRequest('Unable to return diff, unknown file.')); }

	// Verify Resource existence
	const ResourceItem = await ResourcesModel.one(resource.id)
	.catch(err => { return next(err); });

	// TODO. Verify Commit existence.

	// Get raw data from usercontent
	return lib.getRaw(req, res, next, config.services.usercontent, ResourceItem, commit.id, fileName);
};
