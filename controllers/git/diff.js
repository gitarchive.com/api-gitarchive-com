/**
 * Handle queries to return diff of two commits from a specific resource.
 * 
 * @param resource_id (mandatory),
 * @param first_commit_id (mandatory),
 * @param second_commit_id (mandatory)
 * 
 * @returns: DiffItem
 */

const ResourcesModel = require('../../models/resources');
const CommitsModel = require('../../models/commits');
const UserContentModel = require('../../models/usercontent');
const lib = require('gitarchive_lib');
const APIError = lib.error;

module.exports = async function (req, res, next) {

	const resource 	= {}, fromCommit = {}, toCommit = {};
	resource.id 	= req.params.resource_id;
	fromCommit.id 	= req.params.from_commit_id;
	toCommit.id 	= req.params.to_commit_id;
	
	const fileName = req.params.filename;

	// Validate location param (url)
	if (!resource.id)
		{ return next(APIError.badRequest('Resource\'s id is missing.')); }
	
	if (resource.id.length !== 64)
		{ return next(APIError.badRequest('Resource\'s id is malformed.')); }
	
	// Validate from_commit param
	if (!fromCommit.id)
		{ return next(APIError.badRequest('From commit\'s id is missing.')); }

	if (fromCommit.id.length !== 40)
		{ return next(APIError.badRequest('From commit\'s id is malformed.')); }
	
	// Validate to_commit param
	if (!toCommit.id)
		{ return next(APIError.badRequest('To commit\'s id is missing.')); }

	if (toCommit.id.length !== 40)
		{ return next(APIError.badRequest('To commit\'s id is malformed.')); }
	
	// Validate filename param
	if (!fileName || !(["body", "headers", "dom"].includes(fileName)))
		{ return next(APIError.badRequest('Unable to return diff, unknown file.')); }

	// Verify Resource existence
	const ResourceItem = await ResourcesModel.one(resource.id)
	.catch(err => { return next(err); });

	// TODO. Verify FromCommit existence.
	// TODO. Verify ToCommit existence.

	// Get diff from usercontent
	UserContentModel.getDiff(ResourceItem, fromCommit.id, toCommit.id, fileName)
	.then(UsercontentDiffItem => {
		if (UsercontentDiffItem.error)
			{ throw APIError.badImplementation('Diff item has failed'); }
		
		UsercontentDiffItem.object = "diff";
		return res.success(UsercontentDiffItem);
	})
	.catch(next); // Handle errors with express error middleware
    
};
