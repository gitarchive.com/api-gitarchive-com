/**
 * Handle queries to return raw data of a specific resources.
 * 
 * @param: resource_id (mandatory)
 * @param: commit_hash (optional)
 * 
 * @returns: RAW DATA (not a json)
 */

const ResourcesModel = require('../../models/resources');
const UserContentModel = require('../../models/usercontent');
const config = require("../../config")
const lib = require('gitarchive_lib');
const APIError = lib.error;
const logger = lib.logger;

module.exports = async function (req, res, next) {

	const resource 	= {};
	resource.id = req.params.resource_id;
	
	const fileName = req.params.filename;
	
	// Validate resource's id param (url)
	if (!resource.id)
		{ return next(APIError.badRequest('Resource\'s id is missing.')); }
	
	if (resource.id.length !== 64)
		{ return next(APIError.badRequest('Resource\'s id is malformed.')); }
	
	// Validate filename param
	if (!fileName || !(["body", "headers", "dom"].includes(fileName)))
		{ return next(APIError.badRequest('Unable to set raw data, unknown file.')); }

	// Validate that req.body is a Buffer
	if (!Buffer.isBuffer(req.body))
		{ return next(APIError.badRequest('Unable to set raw data, Content-Type shall be of type "application/octet-stream".')); }

	// Verify Resource existence
	const ResourceItem = await ResourcesModel.one(resource.id)
	.catch(err => { return next(APIError.serviceUnavailable("Can't check resource existance.", err)); });
	
	// Set raw data from usercontent
	return lib.setRaw(req, res, next, config.services.usercontent, ResourceItem, fileName);
};
