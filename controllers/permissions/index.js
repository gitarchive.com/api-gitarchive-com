const config 			= require("../../config.js");
const lib			= require('gitarchive_lib');
const APIError			= lib.error;
const logger 			= lib.logger;

const AuthenticationModel = require('../../models/authentication');

module.exports = function (req, res, next) {

	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");

	/*
	 *	Do not send user another error message than: Authentication failed
	 *	We don't want the route be visible from outside
	 */
	if (res.locals.role !== "service")
		return next(APIError.unauthorized("Authentication failed."));

	if (req.method === "OPTIONS")
		return res.status(200).end();
	else
		next();
};
