--
-- PostgreSQL database dump
--

--
-- Name: commits; Type: TABLE; Schema: public;
--

CREATE TABLE public.commits (
    pk SERIAL PRIMARY KEY,
    resource_pk integer,
    fetch_pk integer,
    time_committed timestamp without time zone,
    id character(40)
);

--
-- Name: fetchs; Type: TABLE; Schema: public;
--

CREATE TABLE public.fetches (
    pk SERIAL PRIMARY KEY,
    resource_pk integer,
    status_code smallint,
    content_length integer,
    content_type smallint,
	supplier integer,
    time_requested timestamp without time zone,
	time_responded timestamp without time zone,
    time_completed timestamp without time zone
);

--
-- Name: resources; Type: TABLE; Schema: public;
--

CREATE TABLE public.resources (
    pk SERIAL PRIMARY KEY,
	usercontent_id smallint,
    time_created timestamp without time zone,
	id character(64),
	url text,
    UNIQUE(id)
);

--
-- Name: api_key; Type: TABLE; Schema: public;
--

CREATE TYPE roles AS ENUM ('user', 'service');

CREATE TABLE public.api_keys (
    pk SERIAL PRIMARY KEY,
    api_key character(60) NOT NULL,
    email character(50),
    role roles default 'user',
    time_created timestamp without time zone,

    UNIQUE(api_key)
);

--
-- Name: users_resources value; Type: TABLE; Schema: public;
--

CREATE TABLE public.users_resources (
    pk SERIAL PRIMARY KEY,
    api_keys_pk integer,
    resources_pk integer,
    fetch_time integer NOT NULL,
    time_created timestamp without time zone
);
