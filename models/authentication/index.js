const lib = require('gitarchive_lib');
const APIError = lib.error;

const Database = require('../../lib/database');

module.exports = {

	getApiKeyInfo: function (apiKey) {

		return new Promise((resolve, reject) => {
			const sqlQuery = "SELECT pk, api_key, email, role"
			+ " " + "FROM api_keys"
			+ " " + "WHERE api_key = $1";

			return Database.oneOrNone(sqlQuery, [apiKey])
			.then(dbResult => {
				if (dbResult)
					return resolve(dbResult);
				return reject("Invalid api key provided");
			})
			.catch(Database.lib.handleErrors);
		});
	},

	isService: function (api_key) {
		return (APIkeys.find(e => { return (e.key === api_key && e.role === "service"); }));
	}

};
