const lib = require('gitarchive_lib');
const APIError = lib.error;

module.exports = function (supplier) {

	if (typeof supplier == "number") {
		return (supplier >= 0 && supplier < SUPPLIERS.length) ? SUPPLIERS[supplier] : null;
	}
	else if (typeof supplier == "string") {
		const int = SUPPLIERS.indexOf(supplier.trim());
		return int >= 0 ? int : null;
	}
	
	return null;
};
    
/* 
 * DO NOT CHANGE THE ORDER OF THE ARRAY
 * 
 * Database use an integer (smallint) to save supplier,
 * which integer is based on the following array.
 * 
 * If you wish to add new suppliers to the list,
 * add them add the end of this list (after "internal").
 */

const SUPPLIERS = [

    /* REMINDER, do not change the order of this array */

	"internal",
	"scraperapi",

    /* REMINDER, add new mimetype AFTER this comment */

];
