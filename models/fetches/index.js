const lib = require('gitarchive_lib');
const APIError = lib.error;

const formatting = require('../_formatting');
const formatMimetypes = require('./mimetypes');
const formatSupplier = require('./suppliers');
const Database  = require('../../lib/database');

FetchesModel = {
	
	create: function (ResourceItem, fetchRequest, fetchResponse) {

		const sqlQuery = 'INSERT INTO fetches (resource_pk, status_code, content_length, content_type, supplier, time_requested, time_responded, time_completed)'
		+ ' ' + 'VALUES ($1,$2,$3,$4,$5,$6,$7,$8)'
		+ ' ' + 'RETURNING fetches.pk;'

		const sqlValues = [
			ResourceItem._pk,
			Database.lib.formatNumber(fetchResponse.statusCode),
			Database.lib.formatNumber(fetchResponse.contentLength),
			formatMimetypes(fetchResponse.contentType),
			formatSupplier(fetchRequest.supplier),
			Database.lib.formatDate(fetchRequest.timeRequested),
			Database.lib.formatDate(fetchRequest.timeResponded),
			Database.lib.formatDate(fetchRequest.timeCompleted)
		];
		
		const self = this;

		return Database.one(sqlQuery, sqlValues)
		.then((dbResult) => { return FetchesModel.one(dbResult.pk); })
		.catch(Database.lib.handleErrors);
	},

	one: function (fetch_pk) {

		const sqlQuery = 'SELECT * FROM fetches'
		+ ' ' + 'WHERE fetches.pk = $1'
		+ ' ' + 'LIMIT 1;'

		return Database.oneOrNone(sqlQuery, [fetch_pk])
		.then((dbResult) => {

			if (!dbResult)
				{ throw APIError.notFound("Fetch was not found."); }

			const FetchItem = formatting.fetchItem(dbResult);
			return FetchItem;
		})
		.catch(Database.lib.handleErrors);
	}
};

module.exports = FetchesModel;
