const lib = require('gitarchive_lib');
const APIError = lib.error;

module.exports = function (mime) {

	if (typeof mime == "number") {
		return (mime >= 0 && mime < MIMETYPES.length) ? MIMETYPES[mime] : "";
	}
	else if (typeof mime == "string") {
		const int = MIMETYPES.indexOf((mime.toString().split(";"))[0].trim());
		return int >= 0 ? int : null;
	}
	
	return null;
};
    
/* 
 * DO NOT CHANGE THE ORDER OF THE ARRAY
 * 
 * Database use an integer (smallint) to save mimetype,
 * which integer is based on the following array.
 * 
 * If you wish to add new common mimetype to the list,
 * add them add the end of this list (after "video/x-msvideo").
 */

const MIMETYPES = [

    /* REMINDER, do not change the order of this array */

    "application/EDI-X12",
    "application/EDIFACT",
    "application/epub+zip",
    "application/java-archive",
    "application/javascript",
    "application/json",
    "application/ld+json",
    "application/ms-word",
    "application/msword",
    "application/octet-stream",
    "application/ogg",
    "application/pdf",
    "application/pkcs12",
    "application/rtf",
    "application/typescript",
    "application/vnd.amazon.ebook",
    "application/vnd.apple.installer+xml",
    "application/vnd.mozilla.xul+xml",
    "application/vnd.ms-excel",
    "application/vnd.ms-fontobject",
    "application/vnd.ms-powerpoint",
    "application/vnd.msexcel",
    "application/vnd.mspowerpoint",
    "application/vnd.oasis.opendocument.graphics",
    "application/vnd.oasis.opendocument.presentation",
    "application/vnd.oasis.opendocument.spreadsheet",
    "application/vnd.oasis.opendocument.text",
    "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "application/vnd.visio",
    "application/x-7z-compressed",
    "application/x-abiword",
    "application/x-bzip",
    "application/x-bzip2",
    "application/x-csh",
    "application/x-rar-compressed",
    "application/x-sh",
    "application/x-shockwave-flash",
    "application/x-tar",
    "application/xhtml+xml",
    "application/xhtml+xml",
    "application/xml",
    "application/zip",
    "audio/3gpp",
    "audio/3gpp2",
    "audio/aac",
    "audio/midi",
    "audio/mpeg",
    "audio/ogg",
    "audio/vnd.rn-realaudio",
    "audio/wav",
    "audio/wave",
    "audio/webm",
    "audio/x-ms-wma",
    "audio/x-pn-wav",
    "audio/x-wav",
    "example",
    "font",
    "font/otf",
    "font/ttf",
    "font/woff",
    "font/woff2",
    "image",
    "image/bmp",
    "image/gif",
    "image/jpeg",
    "image/png",
    "image/svg+xml",
    "image/tiff",
    "image/vnd.djvu",
    "image/vnd.microsoft.icon",
    "image/webp",
    "image/x-icon",
    "message",
    "model",
    "multipart",
    "multipart/alternative",
    "multipart/mixed",
    "multipart/related",
    "text",
    "text/calendar",
    "text/css",
    "text/csv",
    "text/html",
    "text/javascript",
    "text/plain",
    "text/xml",
    "video",
    "video/3gpp",
    "video/3gpp2",
    "video/mp4",
    "video/mpeg",
    "video/ogg",
    "video/quicktime",
    "video/web",
    "video/webm",
    "video/x-flv",
    "video/x-ms-wmv",
    "video/x-msvideo",

    /* REMINDER, add new mimetype AFTER this comment */

];
