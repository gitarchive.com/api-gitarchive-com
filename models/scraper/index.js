const lib = require('gitarchive_lib');
const APIError = lib.error;

const formatting = require('../_formatting');
const Database  = require('../../lib/database');

const SQL_RESOURCE_URL 	= "resources.url as r_url";
const SQL_RESOURCE_ID 	= "resources.id as r_id";
const SQL_RESOURCE_INT 	= "resources.pk as r_pk";
// const SQL_RESOURCE_TIME_LATESTFETCH = "resources.time_latestfetch as r_time_latestfetch";
const SQL_RESOURCE_TIME_CREATED 	= "resources.time_created as r_time_created";
const SQL_LATEST_COMMIT_ID = "commits.id as c_latest_id";
const SQL_LATEST_COMMIT_TIME_COMMITTED = "commits.time_committed as c_latest_time_committed";

const SQL_RESOURCE_INFORMATION = SQL_RESOURCE_URL
+ "," + SQL_RESOURCE_INT
+ "," + SQL_RESOURCE_ID
// + "," + SQL_RESOURCE_TIME_LATESTFETCH
+ "," + SQL_RESOURCE_TIME_CREATED;

module.exports = {
	getLatest: function () {

		const sqlQuery = 'SELECT'
		+ ' ' + SQL_RESOURCE_INFORMATION
		+ ',' + 'COUNT(*) as noTransaction'
		+ ' ' + 'FROM resources'
		+ ' ' + 'LEFT JOIN ('
		+ ' ' + '' + 'SELECT DISTINCT resource_pk'
		+ ' ' + '' + 'FROM fetches'
		+ ' ' + '' + 'WHERE fetches.time_requested >= NOW() - interval \'24 hour\''
		+ ' ' + ') f ON resources.pk = f.resource_pk'
		+ ' ' + 'WHERE f.resource_pk IS NULL'
		+ ' ' + 'GROUP BY resources.pk'
		+ ' ' + 'LIMIT 250;';
		
		return Database.any(sqlQuery)
		.then(dbResults => {
			const ResourcesList = formatting.resourcesList(dbResults);
			return ResourcesList;
		})
		.catch(Database.lib.handleErrors);
	}
};
