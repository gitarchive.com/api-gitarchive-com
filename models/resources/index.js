const lib = require('gitarchive_lib');
const APIError = lib.error;

const Database  = require('../../lib/database');
const formatting = require('../_formatting');
const UsersResourcesModel = require('../users_resources');

const SQL_RESOURCE_URL 	= "resources.url as r_url";
const SQL_RESOURCE_ID 	= "resources.id as r_id";
const SQL_RESOURCE_INT 	= "resources.pk as r_pk";
// const SQL_RESOURCE_TIME_LATESTFETCH = "resources.time_latestfetch as r_time_latestfetch";
const SQL_RESOURCE_TIME_CREATED 	= "resources.time_created as r_time_created";
const SQL_LATEST_COMMIT_ID = "commits.id as c_latest_id";
const SQL_LATEST_COMMIT_TIME_COMMITTED = "commits.time_committed as c_latest_time_committed";

const SQL_RESOURCE_INFORMATION = SQL_RESOURCE_URL
+ "," + SQL_RESOURCE_INT
+ "," + SQL_RESOURCE_ID
// + "," + SQL_RESOURCE_TIME_LATESTFETCH
+ "," + SQL_RESOURCE_TIME_CREATED;

module.exports = {
	getLatest: function (limit) {
		limit = Database.lib.sqlQueryLimit(limit);
		return makeSelectQuery(limit);
	},

	search: function (limit, query) {
		limit = Database.lib.sqlQueryLimit(limit);
		query = query.toString();
		return makeSelectQuery(limit, query);
	},

	create: function (location, resource_id, usercontent_id) {
		
		const sqlQuery = 'INSERT INTO resources (url, id, usercontent_id, time_created)'
        + ' ' + 'VALUES ($1,$2, $3, NOW())'
        + ' ' + 'ON CONFLICT (id) DO NOTHING RETURNING id;';

		return Database.one(sqlQuery, [location, resource_id, usercontent_id])
		.then((dbResult) => { return dbResult })
		.catch(Database.lib.handleErrors);
	},

	one: function (resourceId) {

		const sqlQuery = "SELECT " + SQL_RESOURCE_INFORMATION
		+ " " + "FROM resources"
		+ " " + "WHERE resources.id = $1"
		+ " " + "LIMIT 1;";

		return Database.oneOrNone(sqlQuery, [resourceId])
		.then(dbResult => {
			
			if (!dbResult)
				{ throw APIError.notFound("Resource was not found."); }

			const ResourceItem = formatting.resourceItem(dbResult);
			return ResourceItem;
		})
		.catch(Database.lib.handleErrors);
	},

	all: function (limit) {

		limit = Database.lib.sqlQueryLimit(limit);

		const sqlQuery = "SELECT " + SQL_RESOURCE_INFORMATION
		+ " " + "FROM resources"
		+ " " + "LIMIT $1;";

		return Database.any(sqlQuery, limit)
		.then(dbResults => {
			const ResourcesList = formatting.resourcesList(dbResults);
			return ResourcesList;
		})
		.catch(Database.lib.handleErrors);
	},

	random: function () {
		const sqlQuery = "SELECT " + SQL_RESOURCE_INFORMATION
		+ " " + "FROM resources"
		+ " " + "ORDER BY random()"
		+ " " + "LIMIT 1;";

		return Database.one(sqlQuery)
		.then((dbResult) => {

			if (!dbResult)
				throw APIError.badImplementation("Database return no result.");

			const ResourceItem = formatting.resourceItem(dbResult);
			return ResourceItem;
		})
		.catch(Database.lib.handleErrors);
	}
};

async function getQueryTime (sqlQuery, sqlParams) {
	return new Promise((resolve, reject) => {
		Database.any("EXPLAIN ANALYSE " + sqlQuery, sqlParams)
			.then(dbResults => {
				// Get clean executing time
				let executingTime = dbResults.filter(e => {
					if (e["QUERY PLAN"].indexOf("Execution time") > -1)
						return true;
				});
				const time = executingTime[0]["QUERY PLAN"].match(/[0-9\.]+/);
				if (time[0])
					resolve(time[0]);
				resolve(null);
			})
	});
}

async function makeSelectQuery (limit, search) {

	const sqlQuery = "SELECT " + SQL_RESOURCE_INFORMATION
		+ "," + SQL_LATEST_COMMIT_ID + "," + SQL_LATEST_COMMIT_TIME_COMMITTED
		+ ","
			// Add total commits of a specific resource to select statement
			+ "(" + "SELECT COUNT(*)"
			+ " " + "FROM commits AS c1"
			+ " " + "WHERE c1.resource_pk = resources.pk"
			+ ")" + " " + "AS c_total"
	+ " " + "FROM resources"
	+ " " + "INNER JOIN commits ON commits.resource_pk = resources.pk"
	+ " " + "WHERE"
		// Return only the latest commits of the resources (if any)
		+ " " + "commits.pk = (SELECT MAX(pk) FROM commits c2 WHERE c2.resource_pk = resources.pk GROUP BY c2.time_committed ORDER BY c2.time_committed DESC LIMIT 1)"
		// Search by resource's url (case insensitive)
		+ (search ? " " + "AND resources.url ILIKE '%$2:value%'" : "")
	// Anyone knows if the order within a group by has any meanings?
	+ " " + "GROUP BY resources.pk, resources.id, resources.url, commits.id, commits.time_committed"
	// Shows first the tiniest url with latest time fetched.
	+ " " + "LIMIT $1";
	
	const sqlParams = (search ? [limit, search] : [limit]);
	
	const queryTime = await getQueryTime(sqlQuery, sqlParams).then(data => { return (data) });

	return Database.any(sqlQuery, sqlParams)
	.then(dbResults => {

		const ResourcesList = formatting.resourcesList(dbResults);
		// ResourcesList.queryTime = (queryTime ? queryTime : null);

		return ResourcesList;
	})
	.catch(Database.lib.handleErrors);
};
