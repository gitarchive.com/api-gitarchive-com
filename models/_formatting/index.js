const mimetypes = require('../fetches/mimetypes');

/**
 * API objects formats
 */

module.exports = {

	resourcesList: function (dbResults) {

		const self = this;

		const ResourcesList = {};
		ResourcesList.object = "resources";
		ResourcesList.data = dbResults.map(e => { return self.resourceItem(e); });

		return ResourcesList;
	},

	userResourceItem: function (dbResult) {

		const userResource = {};
		userResource.object = "userResource";
		userResource._pk = Number(dbResult.u_r_pk);
		userResource.time_created = dbResult.u_r_time_created;

		userResource.id = dbResult.r_id;
		userResource.location = dbResult.r_url;
		userResource.time_created = dbResult.r_time_created;
		userResource.time_latest_fetch = dbResult.r_time_latestfetch;

		return userResource;
	},

	resourceItem: function (dbResult) {

		const resource = {};
		resource.object = "resource";
		resource._pk = Number(dbResult.r_pk);
		resource.id = dbResult.r_id;
		resource.location = dbResult.r_url;
		resource.time_created = dbResult.r_time_created;
		resource.time_latest_fetch = dbResult.r_time_latestfetch;

		if (dbResult.c_total) {
			resource.commits = {};
			resource.commits.total = Number(dbResult.c_total);
		}

		if (dbResult.c_latest_id) {

			if (!resource.commits)
				{ resource.commits = {}; }

			resource.commits.latest = this.latestCommitItem(dbResult);
		}

		return resource;
	},

	commitsList: function (dbResults) {

		const self = this;

		const commits = {};
		commits.object = "commits";
		commits.data = dbResults.map(e => { return self.commitItem(e); });

		return commits;
	},

	commitItem: function (dbResult) {

		const commit = {};
		commit.object = "commit";
		commit.id = dbResult.c_id;
		commit.time_committed = dbResult.c_time_committed;

		return commit;
	},

	latestCommitItem: function (dbResult) {

		const commit = {};
		commit.c_id = dbResult.c_latest_id;
		commit.c_time_committed = dbResult.c_latest_time_committed;

		return this.commitItem(commit);
	},

	fetchesList: function (dbResults) {

		const self = this;

		const fetches = {};
		fetches.object = "commits";
		fetches.data = dbResults.map(e => { return self.fetchItem(e); });

		return fetches;
	},

	fetchItem: function (dbResult) {

		const fetch = {};

		fetch.id = dbResult.id;

		fetch.request = {};
		fetch.request.time_requested = dbResult.time_requested;
		fetch.request.time_completed = dbResult.time_completed;
		fetch.request._supplier = dbResult.supplier;
		
		fetch.response = {};
		fetch.response.status_code = dbResult.status_code;
		fetch.response.content_length = dbResult.content_length;
		fetch.response.content_type = mimetypes(dbResult.content_type);

		return fetch;
	}

};
