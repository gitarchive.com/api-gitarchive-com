const lib = require('gitarchive_lib');
const APIError = lib.error;

const Database = require('../../lib/database');
const formatting = require('../_formatting');

const SQL_COMMIT_ID = "commits.id as c_id";
const SQL_COMMIT_TIME_COMMITTED = "commits.time_committed as c_time_committed";

const SQL_COMMMIT_INFORMATION = SQL_COMMIT_ID
+ "," + SQL_COMMIT_TIME_COMMITTED;

module.exports = {

	all: function (resourceId, limit) {

		limit = Database.lib.sqlQueryLimit(limit);

		// Return all commits corresponding to resource's ID
		let sqlQuery = "SELECT " + SQL_COMMMIT_INFORMATION
		+ " " + "FROM commits"
		+ " " + "INNER JOIN resources on commits.resource_pk = resources.pk"
		//+ " " + "INNER JOIN fetches on commits.fetch_pk = fetches.pk"
		+ " " + "WHERE resources.id = $1"
		+ " " + "ORDER BY commits.time_committed DESC"
		+ " " + "LIMIT $2;";

		return Database.any(sqlQuery, [resourceId, limit])
		.then(dbResults => {
			const ResourcesList = formatting.commitsList(dbResults);
			return ResourcesList;
		})
		.catch(Database.lib.handleErrors);
	},

	one: function (resourceId, commitId) {

		const sqlQuery = "SELECT " + SQL_COMMMIT_INFORMATION
		+ " " + "FROM commits"
		+ " " + "INNER JOIN resources on commits.resource_pk = resources.pk"
		//+ " " + "INNER JOIN fetches on commits.fetch_pk = fetches.pk"
		+ " " + "WHERE resources.id = $1 AND commits.id = $2"
		+ " " + "LIMIT 1;";

		return Database.oneOrNone(sqlQuery, [resourceId, commitId])
		.then(dbResult => {

			if (!dbResult)
				{ throw APIError.notFound("Commit was not found."); }

			const CommitItem = formatting.commitItem(dbResult);
			return CommitItem;
		})
		.catch(Database.lib.handleErrors);
	},

	create: function (resourcePk, commitId, fetchPk) {

		const sqlQuery = 'INSERT INTO commits (resource_pk, id, fetch_pk, time_committed)'
        + ' ' + 'VALUES ($1,$2,NULL,NOW());'

		return Database.none(sqlQuery, [resourcePk, commitId])
		.then((dbResult) => { return dbResult; })
		.catch(Database.lib.handleErrors);
	},

};
