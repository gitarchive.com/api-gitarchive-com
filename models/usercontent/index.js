const http = require('http');
const lib = require('gitarchive_lib');
const Database  = require('../../lib/database');
const config	= require("../../config")

const APIRequest = lib.request;
const APIError = lib.error;

module.exports = {

	createResource: function (ResourceItem) {

		return APIRequest.usercontent(config.services.usercontent, ResourceItem).createResource();
	},

	createCommit: function (ResourceItem) {

		return APIRequest.usercontent(config.services.usercontent, ResourceItem).createCommit();
	},

	getDiff: function (ResourceItem, from_commit_id, to_commit_id, fileName) {

		return APIRequest.usercontent(config.services.usercontent, ResourceItem).getDiff(from_commit_id, to_commit_id, fileName);
	}
};
